import gtk
import gobject
from gtk import gdk
import os, re, threading
import random


class Member():
    def __init__(self, buttons, key_labels, text_view, name, max_prime = 1000):
        self.name = name
        self.p, self.q, self.n, self.e, self.d = 0,0,0,0,0
        self.random_primes = self.rwh_primes1( max_prime )
        [ self.p, self.q ] = random.sample ( self.random_primes, 2 )
        self.n = self.p * self.q
        
        self.euler_y = (self.p - 1) * (self.q - 1)
        #print self.name,self.euler_y
        #self.primes_eueler = self.rwh_primes1( self.euler_y )
        self.e = random.choice( self.rwh_primes1( self.euler_y ) )
        #print self.name, self.e
        
        self.d = self.generate_d1()
        
        self.pub_key_label = key_labels[ 0 ]
        self.priv_key_label = key_labels[ 1 ]
        self.msg_view = text_view
        
        self.cursor_send_to = None
        
        self.msg = None
        self.buff = None
        
        self.obtained_public_keys = {}
        
        self.pub_key_label.set_markup( "<span font_desc='11.5'>{ " + str( self.e ) + ", " + str( self.n ) + " }</span>" )
        self.priv_key_label.set_markup( "<span font_desc='11.5'>{ " + str( self.d ) + ", " + str( self.n ) + " }</span>" )
        
        self.dec_butt, self.enc_butt, self.snd_butt = buttons
        self.dec_butt.connect( 'clicked', self.decrypt_msg )
        self.enc_butt.connect( 'clicked', self.encrypt_msg )
        self.snd_butt.connect( 'clicked', self.send_msg )
        
        self.msg_view.get_buffer().connect( 'paste-done', self.buttons_manager )
        self.msg_view.get_buffer().connect( 'changed', self.buttons_manager )
        
    
        
    #~ def generate_d(self):
        #~ d = 0
        #~ mul_start = self.e * self.n
        #~ while True:
            #~ mul_start += 1
            #~ if mul_start % self.e == 0:
                #~ d = mul_start / self.e
                #~ if mul_start % self.n == 1: return d
                
    def buttons_manager( self, text_buffer, paste=None ):
        start_iter = text_buffer.get_start_iter()
        end_iter = text_buffer.get_end_iter()
        text = text_buffer.get_text( start_iter, end_iter, True )
        if text:
            self.dec_butt.set_sensitive(True)
            self.enc_butt.set_sensitive(True)
            self.snd_butt.set_sensitive(True)
        else:
            self.dec_butt.set_sensitive(False)
            self.enc_butt.set_sensitive(False)
            self.snd_butt.set_sensitive(False)
    
    def get_random_lt1( self ):
		for i in enumerate( self.primes_eueler ):
			if i[1] >= self.euler_y:
				return random.choice( self.primes_eueler[0:i[0]] )
                
    def generate_d1(self):
        self.d = 0
        while True:
            if ( self.d * self.e ) % self.euler_y == 1:
                return self.d
            self.d += 1
    
    def rwh_primes1( self, n ):
        # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
        """ Returns  a list of primes < n """
        sieve = [True] * (n/2)
        for i in xrange(3,int(n**0.5)+1,2):
            if sieve[i/2]:
                sieve[i*i/2::i] = [False] * ((n-i*i-1)/(2*i)+1)
        return [2] + [2*i+1 for i in xrange(1,n/2) if sieve[i]]
        
    def get_public_key(self):
        return ( self.name, self.e, self.n )
        
    def add_public_key( self, member, methods ):
        self.obtained_public_keys[ member[0] ] = ( (member[1],member[2]), methods )
        
    def remove_public_key( self, member_name ):
        del self.obtained_public_keys[ member_name ]
        
    def set_msg( self, msg ):
        self.msg = msg
        
        self.msg_view.get_buffer().set_text( self.msg )
        
    def get_msg( self ):
        text_buffer = self.msg_view.get_buffer()
        start_iter = text_buffer.get_start_iter()
        end_iter = text_buffer.get_end_iter()
        self.msg = text_buffer.get_text( start_iter, end_iter, True )
        return self.msg
    
    def decrypt_msg(self, button):
        self.get_msg()
        
        self.buff = [ int( i ) for i in self.msg.split() ]
        self.buff = [ pow( i, self.d, self.n ) for i in self.buff ]
        self.msg = ''.join( [ chr( i ) for i in self.buff ] )
        
        self.msg_view.get_buffer().set_text( self.msg )
        
    def encrypt_msg( self, button ):
        self.get_msg()
        
        pbk = self.obtained_public_keys[ self.cursor_send_to ][ 0 ]
        self.buff = [ pow( ord( i ), pbk[ 0 ], pbk[ 1 ] ) for i in self.msg ]
        self.msg = ' '.join( [ str( i ) for i in self.buff ] )
        
        self.msg_view.get_buffer().set_text( self.msg )
        
    def set_cursor_to(self, name):
        self.cursor_send_to = name
        
    def send_msg(self, button):
        self.obtained_public_keys[ self.cursor_send_to ][1][0]( self.get_msg() )


gobject.threads_init()

builder = gtk.Builder()
builder.add_from_file('rsa_template.glade')
window = builder.get_object('main_window')
gen_spinner = builder.get_object('gen_spinner')
generate_butt = builder.get_object('generate_butt')

alice_dec_butt, alice_enc_butt, alice_snd_butt = builder.get_object('alice_dec_butt'), builder.get_object('alice_enc_butt'), builder.get_object('alice_snd_butt')

bob_dec_butt, bob_enc_butt, bob_snd_butt = builder.get_object('bob_dec_butt'), builder.get_object('bob_enc_butt'), builder.get_object('bob_snd_butt')

alice_public_key_label, alice_private_key_label = builder.get_object('alice_public_key'), builder.get_object('alice_private_key')
bob_public_key_label, bob_private_key_label = builder.get_object('bob_public_key'), builder.get_object('bob_private_key')

alice_msg_text, bob_msg_text = builder.get_object('alice_msg'), builder.get_object('bob_msg')

alice, bob = None, None

#gen_spinner.set_no_show_all(True)
gen_spinner.hide()

def set_gen_spinner_activity(flg):
    if flg:
        gen_spinner.show()
        gen_spinner.start()
    else:
        gen_spinner.stop()
        gen_spinner.hide()
    return False

def generate_members( names ):
    gobject.idle_add( set_gen_spinner_activity, True)
        
    global alice
    global bob
    
    alice = Member( ( alice_dec_butt, alice_enc_butt, alice_snd_butt ), ( alice_public_key_label, alice_private_key_label ), alice_msg_text, names[ 0 ] )
    bob = Member( ( bob_dec_butt, bob_enc_butt, bob_snd_butt ), ( bob_public_key_label, bob_private_key_label ), bob_msg_text, names[ 1 ] )
    
    bob.add_public_key( alice.get_public_key(), ( alice.set_msg, alice.get_msg ) )
    alice.add_public_key( bob.get_public_key(), ( bob.set_msg, bob.get_msg ) )
    
    bob.set_cursor_to( names[0] )
    alice.set_cursor_to( names[1] )
    
    gobject.idle_add( set_gen_spinner_activity, False)

def generate_butt_callback( widget, data ):
    widget.set_sensitive(False)
    t = threading.Thread( target = generate_members, args = ( data, ) )
    t.start()



generate_butt.connect( 'clicked', generate_butt_callback, ('Alice','Bob') )
window.connect("destroy", gtk.main_quit)
window.set_title("RSA sample (InfoSec_2014, C-75 V.Dragunov)")
window.show_all()
gtk.main()
